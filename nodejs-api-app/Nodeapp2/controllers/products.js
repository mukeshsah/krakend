const User = require("../src/models/registers")

// Get All products
const getAllProducts = async(req, res) => {
    try {
        const products = await User.find();
        res.json(products);
    } catch (error) {
        res.json({ message: error });
    }
};


// Single product
const getAllProductsTesting = async(req, res) => {
    try {
        const product = await User.findOne({ userid: req.params.userid });
        res.json(product);
    } catch (error) {
        res.json({ message: error });
    }
};






module.exports = { getAllProducts, getAllProductsTesting }